import model.Product;
import store.SimpleStore;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleStoreTests {

    private SimpleStore<Product> store;

    @Before
    public void beforeEachTest() {
        store = new SimpleStore<>(Product::getSku);
    }

    @Test
    public void shouldAddAndRetrieveItemsFromCatalogueBasedOnSKU() {
        Product product = new Product("A", 50.0);
        store.addItem(product);
        assertEquals(product, store.getByKey("A"));
    }

    @Test
    public void shouldReturnNullExistentItem() {
        assertEquals(null, store.getByKey("A"));
    }

    @Test
    public void shouldUpdateItemIfSkuExistsInCatalogue() {
        Product product = new Product("A", 50.0);
        Product updatedProduct = new Product("A", 60.0);
        store.addItems(product, updatedProduct);
        assertEquals(updatedProduct, store.getByKey("A"));
    }

}
