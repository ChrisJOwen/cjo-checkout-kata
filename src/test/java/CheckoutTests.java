import exceptions.UnknownProductException;
import model.Product;
import model.Offer;
import store.Offers;
import store.ProductCatalogue;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheckoutTests {
    Checkout checkout;
    ProductCatalogue catalogue;
    Offers offers;

    @Before
    public void beforeEach() {
        offers = new Offers();
        catalogue = new ProductCatalogue();
        catalogue.addItems(new Product("A", 50.0),
                new Product("B", 30.0),
                new Product("C", 20.0),
                new Product("D", 15.0));
        checkout = new Checkout(catalogue);
    }

    @Test(expected = UnknownProductException.class)
    public void shouldThrowIfItemNotInCatalogue() {
        checkout.beginTransaction(offers).addItem("Z");
    }

    @Test
    public void shouldCalculateTheCostOfASingleItem() {
        Double total = checkout.beginTransaction(offers)
                .addItem("A")
                .calculateTotal();
        assertEquals(50.0, total, 0.0);
    }

    @Test
    public void shouldCalculateTheCostOfTwoSingleItems() {
        Double total = checkout.beginTransaction(offers)
                .addItems("A", "B")
                .calculateTotal();
        assertEquals(80.0, total, 0.0);
    }

    @Test
    public void shouldCalculateTheCostOfMultiItems() {
        Double total = checkout.beginTransaction(offers)
                .addItems("A", "A")
                .calculateTotal();
        assertEquals(100.0, total, 0.0);
    }

    @Test
    public void shouldCalculateTheCostOfMultiItemsOutOfOrder() {
        Double total = checkout.beginTransaction(offers)
                .addItems("A", "B", "A")
                .calculateTotal();
        assertEquals(130.0, total, 0.0);
    }

    @Test
    public void shouldCalculateSingleOffer() {
        offers.addItem(new Offer("A", 50.0, 2));
        Double total = checkout.beginTransaction(offers)
                .addItems("A", "A")
                .calculateTotal();
        assertEquals(50.0, total, 0.0);
    }

    @Test
    public void shouldCalculateMixedSingleOfferAndNormal() {
        offers.addItem(new Offer("A", 50.0, 2));
        Double total = checkout.beginTransaction(offers)
                .addItems("A", "A", "A")
                .calculateTotal();
        assertEquals(100.0, total, 0.0);
    }

    @Test
    public void shouldCalculateRepeatingSingleOffers() {
        offers.addItem(new Offer("A", 50.0, 2));
        Double total = checkout.beginTransaction(offers)
                .addItems("A", "A", "A", "A")
                .calculateTotal();
        assertEquals(100.0, total, 0.0);
    }

    @Test
    public void shouldCalculateMultipleOffers() {
        offers.addItem(new Offer("A", 50.0, 2));
        offers.addItem(new Offer("B", 50.0, 2));

        Double total = checkout.beginTransaction(offers)
                .addItems("A", "B", "A", "B", "A", "A")
                .calculateTotal();
        assertEquals(150.0, total, 0.0);
    }

    @Test
    public void shouldCalculateRepeatingMultipleOffersWithNonOffer() {
        offers.addItem(new Offer("A", 50.0, 2));
        offers.addItem(new Offer("B", 80.0, 3));

        Double total = checkout.beginTransaction(offers)
                .addItems("A", "B", "B", "D", "B", "A", "A", "A", "C", "A")
                .calculateTotal();
        assertEquals(265.0, total, 0.0);
    }



}