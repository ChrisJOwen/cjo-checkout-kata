import exceptions.UnknownProductException;
import model.Product;
import model.Offer;
import store.Offers;
import store.ProductCatalogue;

import java.util.concurrent.ConcurrentHashMap;

public class Checkout {
    private final ProductCatalogue catalogue;

    public Checkout(ProductCatalogue catalogue) {
        this.catalogue = catalogue;
    }

    public Transaction beginTransaction(Offers offers) {
        return new Transaction(catalogue, offers);
    }

    public class Transaction {
        private final ProductCatalogue catalogue;
        private final Offers offers;
        private final ConcurrentHashMap<Product, Integer> items;

        public Transaction(ProductCatalogue catalogue, Offers offers) {
            this.catalogue = catalogue;
            this.offers = offers;
            this.items = new ConcurrentHashMap<>();
        }

        public Transaction addItem(String sku) {
            Product product = catalogue.getByKey(sku);
            if(product==null) throw new UnknownProductException(sku);
            Integer total = 0;
            if (items.containsKey(product)) {
                total = items.get(product);
            }
            items.put(product, total + 1);
            return this;
        }

        public Double calculateTotal() {
            return items.entrySet().stream()
                    .mapToDouble((item) -> calculateItemTotal(item.getKey(), item.getValue()))
                    .sum();
        }

        private Double calculateItemTotal(Product product, Integer amount) {
            Offer offer = offers.getByKey(product.getSku());
            if (offer == null) {
                return product.getCostForQuantity(amount);
            }
            Integer numberOfOfferItems = amount / offer.getRequiredItems();
            Integer numberOfNoneOfferItems = amount % offer.getRequiredItems();
            Double costOfOfferItems = numberOfOfferItems * offer.getAmount();
            Double costOfNoneOfferItems = numberOfNoneOfferItems * product.getCost();
            return costOfNoneOfferItems + costOfOfferItems;
        }

        public Transaction addItems(String... items) {
            for (String item : items) {
                addItem(item);
            }
            return this;
        }
    }
}
