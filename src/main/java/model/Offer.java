package model;

public class Offer {
    private String sku;
    private double amount;
    private int requiredItems;

    public Offer(String sku, double amount, int requiredItems) {
        this.sku = sku;
        this.amount = amount;
        this.requiredItems = requiredItems;
    }

    public double getAmount() {
        return amount;
    }

    public int getRequiredItems() {
        return requiredItems;
    }

    public String getSku() {
        return sku;
    }
}
