package model;

public class Product {
    private String sku;
    private Double cost;

    public Product(String sku, Double cost) {
        this.sku = sku;
        this.cost = cost;
    }

    public Double getCost() {
        return cost;
    }

    public String getSku() {
        return sku;
    }

    public Double getCostForQuantity(Integer quantity) {
        return this.cost * quantity;
    }
}
