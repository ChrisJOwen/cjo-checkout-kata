package exceptions;

public class UnknownProductException extends RuntimeException {
    public UnknownProductException(String sku) {
        super("Unknown product ".concat(sku));
    }
}
