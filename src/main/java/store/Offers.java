package store;


import model.Offer;

public class Offers extends SimpleStore<Offer> {
    public Offers() {
        super(Offer::getSku);
    }
}
