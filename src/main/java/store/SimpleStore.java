package store;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class SimpleStore<T> {
    private final ConcurrentHashMap<String, T> items = new ConcurrentHashMap<>();
    private final Function<T, String> key;

    public SimpleStore(Function<T, String> key){
        this.key = key;
    }

    public SimpleStore<T> addItem(T item) {
        items.put(key.apply(item), item);
        return this;
    }

    public SimpleStore<T>  addItems(T ...items) {
        for(T item: items){
            addItem(item);
        }
        return this;
    }

    public T getByKey(String key) {
        return items.get(key);
    }
}