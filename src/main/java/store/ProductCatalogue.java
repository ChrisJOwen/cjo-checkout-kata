package store;

import model.Product;

public class ProductCatalogue extends SimpleStore<Product>  {
    public ProductCatalogue() {
        super(Product::getSku);
    }
}
