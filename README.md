Checkout Kata
----------------

This is a very simplified checkout only meeting the requirements specified in the kata, doesn't generate invoices or anything fancy. 
I have purposely not gone all enterprisy, there are no services, DI or Controller layers. This is not because I am unaware of such common patterns
its because I wouldn't start adding these abstractions in real life until they required.

Assumptions:
    - There was no mention as to whether offers are repeatable, I assumed that they were. For example for the offer "2 fo 10" 
      if there were 4 items of this type I assumed that the offer would be applicable twice i.e. totalling 20
    - No mention of currency handling and conversion from pennies to pounds, left this simply calculating the pence value with no rounding
      

Run Me
----------------

This project uses gradle, to run the tests install gradle: https://docs.gradle.org/current/userguide/installation.html and run 

    gradle cleanTest test
